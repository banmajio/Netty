package com.banmajio.io;

import java.io.IOException;
import java.net.Socket;
import java.util.Date;

/**
 * @ClassName: IOClient
 * @Description:传统IO编程客户端
 * @author: banmajio
 * @date: 2019年11月4日 上午9:44:47
 * @Copyright: banmajio
 */
public class IOClient {
	public static void main(String[] args) {
		new Thread(() -> {
			try {
				Socket socket = new Socket("127.0.0.1", 8000);
				while (true) {
					try {
						socket.getOutputStream().write((new Date() + ":hello world").getBytes());
						socket.getOutputStream().flush();
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}).start();
	}
}
