package com.banmajio.nettytcp1;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NettyServer {
	private static final int port = 8000;// 设置服务端端口
	private static EventLoopGroup group = new NioEventLoopGroup();// 通过NIO方式来接收连接和处理连接
	private static ServerBootstrap b = new ServerBootstrap();

	public static void main(String[] args) {
		try {
			b.group(group);
			b.channel(NioServerSocketChannel.class);
			b.childHandler(new NettyServerFilter());// 设置过滤器
			// 服务器绑定端口监听
			ChannelFuture f = b.bind(port).sync();
			System.out.println("服务端启动成功！");
			f.channel().closeFuture().sync();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			group.shutdownGracefully();// 关闭group，释放掉所有资源包括创建的线程
		}
	}
}
