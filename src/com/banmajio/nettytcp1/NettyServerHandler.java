package com.banmajio.nettytcp1;

import java.net.Inet4Address;
import java.util.Date;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class NettyServerHandler extends SimpleChannelInboundHandler<String> {

	/*
	 * 收到消息时，返回信息
	 */
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
		System.out.println("服务端收到的消息是：" + msg);
		if ("quit".equals(msg)) {
			ctx.close();
		}
		Date date = new Date();
		ctx.writeAndFlush(date);
//		ctx.writeAndFlush(date + "\n");
	}

	/*
	 * 建立连接时，返回消息
	 */
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		System.out.println("连接的客户端地址：" + ctx.channel().remoteAddress());
		ctx.writeAndFlush("客户端" + Inet4Address.getLocalHost().getHostAddress() + "成功与服务端建立连接！ \n");
		super.channelActive(ctx);
	}

}
