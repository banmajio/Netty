package com.banmajio.nettytcp1;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

public class NettyClient {
	private static final int port = 8000;// 客户端端口
	private static String host = "127.0.0.1";// ip地址
	private static EventLoopGroup group = new NioEventLoopGroup();// 通过NIO方式接收连接和处理连接
	private static Bootstrap b = new Bootstrap();
	private static Channel ch;

	public static void main(String[] args) {
		System.out.println("客户端启动成功！");
		try {
			b.group(group);
			b.channel(NioSocketChannel.class);
			b.handler(new NettyClientFilter());
			// 连接服务器
			ch = b.connect(host, port).sync().channel();
			String str = "hello";
			ch.writeAndFlush(str + "\n");
			System.out.println("客户端发送数据" + str);
			ch.closeFuture().sync();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			group.shutdownGracefully();
		}
	}

}
