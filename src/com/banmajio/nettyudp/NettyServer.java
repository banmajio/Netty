package com.banmajio.nettyudp;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;

/**
 * @ClassName: NettyServer
 * @Description:netty 服务端
 * @author: banmajio
 * @date: 2019年11月5日 下午2:31:33
 * @Copyright: banmajio
 */
public class NettyServer {
	public static void main(String[] args) {
		EventLoopGroup group = new NioEventLoopGroup();
		try {
			Bootstrap b = new Bootstrap();
			b.group(group).channel(NioDatagramChannel.class).option(ChannelOption.SO_BROADCAST, true)// 广播
					.option(ChannelOption.SO_RCVBUF, 2048 * 1024)// 设置UDP读缓存区为2M
					.option(ChannelOption.SO_SNDBUF, 1024 * 1024)// 设置UDP写缓存区为1M
					.handler(new MyServerChannelInitializer());

			ChannelFuture f = b.bind(8001).sync();
			System.out.println("server发送完毕==========");
			f.channel().closeFuture().sync();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			group.shutdownGracefully();
		}
	}
}
