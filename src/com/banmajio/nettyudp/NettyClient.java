package com.banmajio.nettyudp;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.nio.NioDatagramChannel;

/**
 * @ClassName: NettyClient
 * @Description:Netty客户端
 * @author: banmajio
 * @date: 2019年11月5日 上午11:07:32
 * @Copyright: banmajio
 */
public class NettyClient {
	public static void main(String[] args) {
		EventLoopGroup group = new NioEventLoopGroup();
		try {
			Bootstrap b = new Bootstrap();
			b.group(group).channel(NioDatagramChannel.class).handler(new MyClientChannelInitializer());
			Channel ch = b.bind(8000).sync().channel();
			// 向目标端口发送消息
			/**
			 * DatagramPacket()创建要发送的数据包 Charset 字符集
			 */
			ch.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer("你好端口8000，我是01收到请回答！", Charset.forName("GBK")),
					new InetSocketAddress("127.0.0.1", 8001))).sync();
			ch.closeFuture().sync();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			group.shutdownGracefully();
		}
	}
}
