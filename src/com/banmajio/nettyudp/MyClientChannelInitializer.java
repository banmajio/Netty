package com.banmajio.nettyudp;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.nio.NioDatagramChannel;

/**
 * @ClassName: MyChannelInitializer
 * @Description:客户端过滤器 ChannelInitializer：一种特殊的channelinboundhandler，它提供了一种简单的方法，可以在通道注册到其eventloop后对其进行初始化。
 * @author: banmajio
 * @date: 2019年11月5日 下午2:15:48
 * @Copyright: banmajio
 */
public class MyClientChannelInitializer extends ChannelInitializer<NioDatagramChannel> {

	@Override
	protected void initChannel(NioDatagramChannel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();
		// 解码转String，注意调整自己的编码格式
		pipeline.addLast(new MyClientHandler());
	}

}
