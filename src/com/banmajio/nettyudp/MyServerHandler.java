package com.banmajio.nettyudp;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;

public class MyServerHandler extends SimpleChannelInboundHandler<DatagramPacket> {

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket packet) throws Exception {
		String msg = packet.content().toString(Charset.forName("GBK"));
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " UDP服务端接收到消息：" + msg);

		// 向客户端发送消息
		String json = "你好01，我是02，收到收到！！！！";
		byte[] bytes = json.getBytes(Charset.forName("GBK"));
		DatagramPacket data = new DatagramPacket(Unpooled.copiedBuffer(bytes), packet.sender());
		ctx.writeAndFlush(data);
	}
}
