package com.banmajio.nettyudp;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;

/**
 * @ClassName: MyServerChannelInitializer
 * @Description:TODO
 * @author: banmajio
 * @date: 2019��11��5�� ����3:07:46
 * @Copyright: banmajio
 */
public class MyServerChannelInitializer extends ChannelInitializer<NioDatagramChannel> {

	private EventLoopGroup group = new NioEventLoopGroup();

	@Override
	protected void initChannel(NioDatagramChannel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();
		pipeline.addLast(group, new MyServerHandler());
	}

}
