package com.banmajio.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDP_server01 {
	public static void main(String[] args) throws Exception {
		DatagramSocket socket = new DatagramSocket(8080, InetAddress.getByName("192.168.0.44"));
		byte[] arr = new byte[1024];
		DatagramPacket packet = new DatagramPacket(arr, arr.length);
		while(true) {
			socket.receive(packet);
			byte[] arr1 = packet.getData();
			System.out.println("server01" + new String(arr1));
		}
//		socket.close();
	}
}
